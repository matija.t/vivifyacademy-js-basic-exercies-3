/* 2. Modelirati jednostavan sistem bankarstva: 
    Napraviti konstruktor Racun koja sadrži atribute brojRacuna, banka, vlasnik i stanje.
    Konstruktor Racun prima četiri argumenta, koji odgovaraju gorepomenutim atributima, i setuje ih.
    Napraviti konstruktor Banka koja sadrži kao atribute string ime i niz racuni.
    U konstruktoru Banka dodati metodu dodajRacun, koja:
        kao argumente prima ime vlasnika, pocetno stanje i broj racuna
        pravi novi objekat Racun koji inicijalizuje pomoću podataka koji su prosleđeni
        ubacuje novostvoreni račun u svoj niz racuni i vraća ga.
*/

function Racun(brojRacuna, banka, vlasnik, stanje) {
  this.brojRacuna = brojRacuna;
  this.banka = banka;
  this.vlasnik = vlasnik;
  this.stanje = stanje;
}

function Bank(ime) {
  this.ime = ime;
  this.racuni = [];

  this.dodajRacun = function (imeVlasnika, pocetnoStanje, brojRacuna) {
    var noviRacun = new Racun(brojRacuna, this, imeVlasnika, pocetnoStanje);
    this.racuni.push(noviRacun);

    return dodajRacun
  }
}
