/* 4. Napisati konstruktor Vozilo, čiji objekti treba da imaju atribut tip i metod vozi.
    Napisati konstruktor Automobil čiji objekti imaju postavljen tip "Automobil" i nasleđuju metod vozi od Vozila.
*/

class Vozilo {
    constructor(tip) {
        this.tip = tip;
    }

    vozi() {
        console.log('vozi');
    }
}


class Automobil extends Vozilo {
    constructor() {
        super('Automobil');
    }
}
