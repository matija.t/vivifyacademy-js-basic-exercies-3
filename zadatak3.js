/* 3. Kreirati novu sliku u JavaScriptu, ali koristeći Image konstruktor umesto document.createElement (https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/Image).
    Postaviti mu src na http://amolife.com/image/images/stories/Animals/cute_kitten_12.jpg i dodati ga u dokument pomoću document.body.appendChild.
*/

var image = new Image();
image.src = 'http://amolife.com/image/images/stories/Animals/cute_kitten_12.jpg';
document.body.appendChild(image);
