/* 5. Napisati klasu Cetvorougao čiji konstruktor prihvata atribute a, b, c i d i postavlja ih na objekat.
    Objekti treba da imaju metod izracunajObim, koji sabira vrednosti a, b, c i d.
*/

class Cetvorougao {
    constructor (a, b, c, d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    izracunajObim() {
        return this.a + this.b + this.c + this.d;
    }
}


/* 6. Napisati klasu Pravougaonik koja nasleđuje od Cetvorougla, gde konstruktor prihvata atribute a i b, postavlja c na istu vrednost kao a, d na istu vrednost kao b.
    Pored toga, treba da postoji metod izracunajPovrsinu, koja vraća proizvod a i b.
*/

class Pravougaonik extends Cetvorougao {
    constructor (a, b) {
        super(a, b, a, b);
    }

    izracunajPovrsinu() {
        return this.a * this.b;
    }
}
